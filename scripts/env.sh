#!/bin/bash

get_script_dir () {
     SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd "$( dirname "$SOURCE" )" && pwd )"
     echo "$DIR"
}

VIRTUALENV_DIR=`/usr/bin/realpath -L $(get_script_dir)/../env`

# Parameters
#PACKAGE_MAIN_NAME=lacapp
PACKAGE_SITE_NAME=lac
PROJECT_NAME=lacprj

# Activate the virtual environment
source $VIRTUALENV_DIR/bin/activate

# Django project directory
DJANGOPROJECT=$VIRTUALENV_DIR/$PROJECT_NAME
DJANGOPROJECTAPPS=$DJANGOPROJECT/$PACKAGE_SITE_NAME/apps
DJANGO_SETTINGS_MODULE=conf.settings
DJANGO_WSGI_MODULE=$PACKAGE_SITE_NAME.wsgi

# Pythonpath
PYTHONPATH=$DJANGOPROJECTAPPS:$DJANGOPROJECT:$VIRTUALENV_DIR

# export
export PYTHONPATH=$PYTHONPATH
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
