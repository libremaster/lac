#!/bin/bash

. `/usr/bin/dirname $0`/env.sh

# mise a jour database
echo "python ${DJANGOPROJECT}/manage.py migrate --noinput"
python ${DJANGOPROJECT}/manage.py migrate --noinput
