#!/bin/bash

. `/usr/bin/dirname $0`/env.sh

exec `/usr/bin/dirname $0`/../env/bin/gunicorn $DJANGO_WSGI_MODULE:application \
    --workers 3 \
    --timeout 60 \
    --error-logfile=`/usr/bin/dirname $0`/../logs/gunicorn.log \
    --bind=unix:/run/lac/gunicorn.sock \
    --log-level info