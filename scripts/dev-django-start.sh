#!/bin/bash

. `/usr/bin/dirname $0`/env.sh

cd $DJANGOPROJECT

python3 manage.py runserver 0.0.0.0:8000
