#!/bin/bash

. `/usr/bin/dirname $0`/env.sh

# effacement du cache
echo "find ${VIRTUALENV_DIR} -name __pycache__ -exec rm -rf {} \\;"
find ${VIRTUALENV_DIR} -name __pycache__ -exec rm -rf {} \\;

# mise a jour fichier static
echo "python ${DJANGOPROJECT}/manage.py collectstatic --noinput"
python ${DJANGOPROJECT}/manage.py collectstatic --noinput

# touch sur le fichier wsgi.py
echo "touch ${DJANGOPROJECT}/${PACKAGE_SITE_NAME}/wsgi.py"
touch ${DJANGOPROJECT}/${PACKAGE_SITE_NAME}/wsgi.py

# reload
echo "sudo /usr/bin/systemctl reload lac.service"
sudo /usr/bin/systemctl reload lac.service
