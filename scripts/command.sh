#!/bin/bash -x

. `/usr/bin/dirname $0`/env.sh

cd $DJANGOPROJECT

python manage.py "$@"
