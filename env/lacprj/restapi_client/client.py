# -*- encoding: utf-8 -*-

# python
import requests
import requests.auth
import json
from base64 import b64decode, b64encode
import datetime
from time import sleep
requests.packages.urllib3.disable_warnings()

class ResponseError(Exception):

    def __init__(self, response):
        self.response = response

class ApiError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class Api:

    def __init__ (
        self, hoster_url,
        hoster_uid_kname='LOGIN',
        hoster_api_key_kname='TOKEN',
        hoster_uid=None,
        hoster_api_key=None,
        content_type='application/json',
        accept='application/json',
        debug=False
    ):
        self.debug = debug
        self.api_url = hoster_url
        self.hoster_uid_kname = hoster_uid_kname
        self.hoster_uid = hoster_uid
        self.hoster_api_key_kname = hoster_api_key_kname
        self.hoster_api_key = hoster_api_key
        self.content_type = content_type
        self.accept = accept
        self.start = datetime.datetime.now()
        self.end = self.start
        self.diffmicroseconds = 500000

    def get_token(self,
        path,
        auth_basic_id,
        auth_basic_secret,
        hoster_uid_kname="username",
        hoster_password_kname="password",
        hoster_uid=None,
        hoster_password=None
    ):
        # prepare datas
        datas = {"grant_type": "password", hoster_uid_kname: hoster_uid, hoster_password_kname: hoster_password}

        # made request
        client_auth = requests.auth.HTTPBasicAuth(auth_basic_id, auth_basic_secret)
        response = self.post(path, auth=client_auth, datas=datas)
        return (response['token_type'],response['access_token'])

    def debug_response(self, response):
        print ('========================================= REQUEST =========================================')
        print ('----------------------------------------- METHOD:')
        print (response.request.method)
        print ('----------------------------------------- URL:')
        print (response.request.url)
        print ('----------------------------------------- HEADER:')
        print ('\n'.join('{}: {}'.format(k, v) for k, v in response.request.headers.items()))
        print ('----------------------------------------- BODY:')
        print (response.request.body)
        print ('========================================= RESPONSE =========================================')
        print ('----------------------------------------- STATUS:')
        print (response.status_code)
        print ('----------------------------------------- HEADERS:')
        print (response.headers)
        print ('----------------------------------------- TEXT:')
        print (response.text)

    def get_response (self, method, path, content, params, auth, content_json):
        target_url = self.api_url + path

        headers = {'user_agent' : 'pythonApi', 'Content-Type' : self.content_type, 'Accept' : self.accept}
        if self.hoster_uid:
            headers[self.hoster_uid_kname] = self.hoster_uid
        if self.hoster_api_key:
            headers[self.hoster_api_key_kname] = self.hoster_api_key
        req = getattr(requests, method.lower())
        curdiff = self.end - self.start
        if curdiff.microseconds < self.diffmicroseconds:
            sleep((float(self.diffmicroseconds)-float(curdiff.microseconds))/1000000)
        self.start = datetime.datetime.now()
        response = req(target_url, headers=headers, params=params, data=content, json=content_json, auth=auth, allow_redirects=False, verify=False)
        if self.debug:
            self.debug_response(response)
        return response
        self.end = datetime.datetime.now()

    def rawCall_and_status (self, method, path, content = None, params = None, auth = None, content_json = None):
        response = self.get_response(method, path, content, params, auth, content_json)

        if not 200 <= response.status_code < 300:

            if 100 <= response.status_code < 200:
                raise ResponseError(response)

            if 300 <= response.status_code < 400:
                raise ResponseError(response)

            if 400 <= response.status_code < 500:
                raise ResponseError(response)

            if 500 <= response.status_code:
                raise ResponseError(response)

        try:
            decoded = json.loads(response.text)
        except ValueError:
            if response.text == '':
                return {}
            else:
                raise ApiError('Not json response')

        return decoded

    def get (self, path, datas = None):
        return self.rawCall_and_status("get", path, params = datas)

    def put (self, path, datas = None, datas_json = None):
        return self.rawCall_and_status("put", path, content = datas, content_json = datas_json)

    def post (self, path, datas = None, datas_json = None, auth = None):
        return self.rawCall_and_status("post", path, content = datas, content_json = datas_json, auth = auth)

    def patch (self, path, datas = None):
        return self.rawCall_and_status("patch", path, content = datas)

    def delete (self, path, datas = None):
        return self.rawCall_and_status("delete", path, content = datas)
