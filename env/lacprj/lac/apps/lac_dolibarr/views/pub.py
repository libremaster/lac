# -*- coding: UTF-8 -*-

# python
import stripe

# django
from django.conf import settings
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
# django views
from django.views.generic import TemplateView, View, DetailView, FormView
from django.views.generic.detail import SingleObjectMixin

# django decorators
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt

# django_filters
from django_filters import FilterSet
from django_filters.views import FilterView

# lib
from lac_mailing.lib import seb_api
from lac_dolibarr.lib import api as dol_api
from .home import get_stripe_customer

from lac_auth.forms import CustomUserAccountGoForm
from lac_mailing.forms import PubServiceSqueezeForm

# models
from lac_directory import models as lac_directory_models
from ..models import Contract, Service, Category

# ------
# PAYPAL
# ------
@method_decorator(never_cache, name='dispatch')
class PaypalJsView(TemplateView):
    content_type = 'application/x-javascript; charset=UTF-8'
    template_name = 'lac_dolibarr/paypal.js'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data()
        ctx['paypal_client_id'] = settings.PAYPAL_CLIENT_ID
        if 'facid' in kwargs:
            ctx['url_create'] = reverse('lac_dolibarr_home:invoice-paypal-order-create', kwargs={
                    'facid': kwargs['facid']})
            ctx['url_capture'] = reverse('lac_dolibarr_home:invoice-paypal-order-capture')
            ctx['ur_next'] = reverse('lac_dolibarr_home:invoice-paypal', kwargs={
                    'facid': kwargs['facid']})
        #if 'contractid' in kwargs:
        #    ctx['url'] = reverse('lac_dolibarr_home:contract-paypal-session-create', kwargs={
        #            'contractid': kwargs['contractid']})
        if 'action' in kwargs:
            if kwargs['action'] == 'order':
                if 'slug' in kwargs:
                    ctx['url_create'] = reverse('lac_dolibarr_home:homeservice-order', kwargs={
                        'slug': kwargs['slug']})
                    ctx['url_capture'] = reverse('lac_dolibarr_home:invoice-paypal-order-capture')
                    ctx['url_next'] = reverse('lac_dolibarr_home:orders', kwargs={'socid': self.request.session['order_socid'] })
        return ctx

# ------
# STRIPE
# ------
@method_decorator(never_cache, name='dispatch')
class StripeJsView(TemplateView):
    content_type = 'application/x-javascript; charset=UTF-8'
    template_name = 'lac_dolibarr/stripe.js'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data()
        ctx['stripe_publishable_key'] = settings.STRIPE_API_PK
        if 'facid' in kwargs:
            ctx['url'] = reverse('lac_dolibarr_home:invoice-stripe-session-create', kwargs={
                    'facid': kwargs['facid']})
        if 'contractid' in kwargs:
            ctx['url'] = reverse('lac_dolibarr_home:contract-stripe-session-create', kwargs={
                    'contractid': kwargs['contractid']})
        if 'action' in kwargs:
            if kwargs['action'] == 'order':
                if 'slug' in kwargs:
                    ctx['url'] = reverse('lac_dolibarr_home:homeservice-order', kwargs={
                        'slug': kwargs['slug']})
        return ctx

@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class StripeWebhookView(View):

    def handle_stripe_ev_obj(self, stripe_ev_obj):

        stripe.api_key = settings.STRIPE_API_SK

        if isinstance(stripe_ev_obj['metadata'], dict):

            # payment intent ?
            pi = None
            if 'payment_intent' in stripe_ev_obj and stripe_ev_obj['payment_intent']:
                pi = stripe.PaymentIntent.retrieve(stripe_ev_obj['payment_intent'])
            if stripe_ev_obj['object'] == 'payment_intent':
                pi = stripe_ev_obj

            # création de facture que si PI :
            dol_invoice_id = None
            dol_invoice = None
            if pi and 'dolibarr_order_id' in stripe_ev_obj['metadata']:
                # création facture liée
                dol_invoice = dol_api.invoice_createfromorder(orderid=stripe_ev_obj['metadata']['dolibarr_order_id'])
                dol_invoice_id = dol_invoice['id']
                dol_api.invoice_validate(facid=dol_invoice_id)

            if 'dolibarr_invoice_id' in stripe_ev_obj['metadata']:
                dol_invoice_id = stripe_ev_obj['metadata']['dolibarr_invoice_id']

            if dol_invoice_id:

                payments_recorded = False
                if dol_invoice is None:
                    # vérification si le paiement est déjà enregistré
                    dol_invoice = dol_api.invoice_get(facid=dol_invoice_id, cached=False)
                    for i in range(len(dol_invoice['payments'])):
                        payment = dol_invoice['payments'][i]

                        stripe_search = re.search('^pi_(.*)', payment['num'], re.IGNORECASE)
                        if stripe_search:
                            if payment['num'] == pi['id']:
                                payments_recorded = True

                if not payments_recorded:
                    # enregistrement du PI dans dolibarr
                    arrayofamounts = {}
                    arrayofamounts[dol_invoice_id] = pi['amount']/100

                    datas = {
                        'arrayofamounts': arrayofamounts,
                        'datepaye': str(pi['created']),
                        'paiementid': settings.DOLIBARR_STRIPE_PAIEMENT_ID,
                        'closepaidinvoices': 'yes',
                        'accountid': settings.DOLIBARR_STRIPE_ACCOUNT_ID,
                        'num_paiement': pi['id'],
                    }

                    dol_api.invoice_paymentsdistributed_post(datas=datas)

            # setup intent
            if 'setup_intent' in stripe_ev_obj and stripe_ev_obj['setup_intent'] and 'dolibarr_contract_id' in stripe_ev_obj['metadata']:
                intent = stripe.SetupIntent.retrieve(stripe_ev_obj['setup_intent'])

                dj_contract = Contract.objects.get(dolibarr_contract_id=int(stripe_ev_obj['metadata']['dolibarr_contract_id']))
                if dj_contract.stripe_payment_method == '' or not dj_contract.stripe_payment_method:
                    dj_contract.stripe_payment_method = intent['payment_method']
                    dj_contract.save()

                # nouveau client créé (normalement c'est déjà fait à la création de stripe_ev_obj)
                stripe_customer = get_stripe_customer(contractid=stripe_ev_obj['metadata']['dolibarr_contract_id'])
                pm = stripe.PaymentMethod.retrieve(intent['payment_method'])
                if pm['customer'] is None or pm['customer'] == '':
                    stripe.PaymentMethod.attach(
                        intent['payment_method'],
                        customer=stripe_customer,
                    )

                dol_api.contract_validate(contractid=int(stripe_ev_obj['metadata']['dolibarr_contract_id']))

    def post(self, request, *args, **kwargs):

        # vérification signature stripe
        stripe.api_key = settings.STRIPE_API_SK
        payload = request.body
        sig_header = request.META['HTTP_STRIPE_SIGNATURE']
        event = None

        try:
            event = stripe.Webhook.construct_event(
                payload, sig_header, settings.STRIPE_API_WH
            )
        except ValueError as e:
            # Invalid payload
            return HttpResponse(status=400)
        except stripe.error.SignatureVerificationError as e:
            # Invalid signature
            return HttpResponse(status=400)

        # Handle the checkout.session.completed event
        if event['type'] == 'checkout.session.completed' or event['type'] == 'payment_intent.succeeded':
            stripe_ev_obj = event['data']['object']
            self.handle_stripe_ev_obj(stripe_ev_obj)

        return HttpResponse(status=200)

# ----
# Apps
# ----
class PubServiceFilter(FilterSet):
    class Meta:
        model = Service
        fields = ['category__name']

class PubServiceList(FilterView):
    model = Service
    paginate_by = settings.PAGINATE_BY
    filterset_class = PubServiceFilter
    template_name = 'lac_dolibarr/pubservice_list.html'

    def get_queryset(self):
        if 'category' in self.kwargs:
            return Service.objects.filter(category__name__iexact=self.kwargs['category'], on_sale=True, public=True)
        else:
            return Service.objects.filter(on_sale=True, public=True)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        try:
            ctx['page_js'] = lac_directory_models.Page.objects.get(slug='politique-de-confidentialite-et-mentions-legales')
        except ObjectDoesNotExist:
            pass

        return ctx

class PubServiceView(DetailView):
    model = Service
    template_name = 'lac_dolibarr/pubservice_detail.html'

    def get(self, request, *args, **kwargs):
        super_response = super().get(request, *args, **kwargs)
        if not request.user.is_staff and not self.object.on_sale:
            return HttpResponseForbidden()

        if self.object.ting_lp and self.object.ting_lp != '':
            return HttpResponseRedirect(self.object.ting_lp)
        return super_response

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        try:
            ctx['page_js'] = lac_directory_models.Page.objects.get(slug='politique-de-confidentialite-et-mentions-legales')
        except ObjectDoesNotExist:
            pass

        return ctx

class PubServiceSqueeze(SingleObjectMixin, FormView):
    model = Service
    template_name = 'lac_dolibarr/pubservice_squeeze.html'
    form_class = PubServiceSqueezeForm

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.is_staff and not self.object.on_sale:
            raise PermissionDenied()
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('lac_dolibarr_home:homeservice-order', kwargs={'slug': kwargs['slug']}))
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.is_staff and not self.object.on_sale:
            return HttpResponseForbidden()
        return super().post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data()
        ctx['force_squeeze'] = self.request.GET.get('force_squeeze', False)
        ctx['squeeze'] = self.request.session.get('squeeze_' + str(self.object.id), False)
        ctx['dol_service'] = dol_api.products_get(proid=self.object.dolibarr_product_id)
        ctx['currency'] = settings.DOLIBARR_DEFAULT_CURRENCY

        if self.object.auto_renew():
            ctx['text_rec'] = self.dol_service['duration_value'] + ' ' + self.dol_service['delta'][1]
        else:
            ctx['text_rec'] = ''

        if ctx['text_rec'] != '':
                ctx['button_text_rec'] = ' tous les ' + ctx['text_rec']
        else:
            ctx['button_text_rec'] = ''

        return ctx
    
    def form_invalid(self, request, *args, **kwargs):
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def form_valid(self, form):
        seb_api.contact_add(form.cleaned_data['email'], self.object.sendinblue_list_id)
        self.request.session['squeeze_' + str(self.object.id)] = True
        return HttpResponseRedirect(reverse_lazy('lac_dolibarr_pub:pubservice-squeeze', kwargs={'slug': self.kwargs['slug']}))

class PubServiceOrder(SingleObjectMixin, FormView):
    model = Service
    template_name = 'lac_dolibarr/pubservice_order.html'
    form_class = CustomUserAccountGoForm
    success_url = reverse_lazy('lac_auth_me:thanks')
    token_generator = default_token_generator
    
    def dispatch(self, request, *args, **kwargs):
        self.next_url = reverse_lazy('lac_dolibarr_home:homeservice-order', kwargs={'slug': kwargs['slug']})
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.is_staff and not self.object.on_sale:
            raise PermissionDenied()
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.next_url)
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.is_staff and not self.object.on_sale:
            return HttpResponseForbidden()
        return super().post(request, *args, **kwargs)
    
    def form_invalid(self, request, *args, **kwargs):
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def form_valid(self, form):
        user = form.save(token_generator=self.token_generator, request=self.request)
        if user.is_active:
            messages.add_message(self.request, messages.INFO, "L'email que vous avez saisie possède déjà un compte. Veuillez vous identifier avec pour poursuivre la commande. Merci.")
            return HttpResponseRedirect(self.next_url)
        else:
            self.request.session['next_url'] = self.next_url
            return super().form_valid(form)