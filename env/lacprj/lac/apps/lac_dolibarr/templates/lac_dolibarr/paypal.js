// **** CSRF protection
var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

// convert FORM to JSON
$.fn.formToJson = function () {
  form = $(this);

  var formArray = form.serializeArray();
  var jsonOutput = {};

  $.each(formArray, function (i, element) {
      var elemNameSplit = element['name'].split('[');
      var elemObjName = 'jsonOutput';

      $.each(elemNameSplit, function (nameKey, value) {
          if (nameKey != (elemNameSplit.length - 1)) {
              if (value.slice(value.length - 1) == ']') {
                  if (value === ']') {
                      elemObjName = elemObjName + '[' + Object.keys(eval(elemObjName)).length + ']';
                  } else {
                      elemObjName = elemObjName + '[' + value;
                  }
              } else {
                  elemObjName = elemObjName + '.' + value;
              }

              if (typeof eval(elemObjName) == 'undefined')
                  eval(elemObjName + ' = {};');
          } else {
              if (value.slice(value.length - 1) == ']') {
                  if (value === ']') {
                      eval(elemObjName + '[' + Object.keys(eval(elemObjName)).length + '] = \'' + element['value'].replace("'", "\\'") + '\';');
                  } else {
                      eval(elemObjName + '[' + value + ' = \'' + element['value'].replace("'", "\\'") + '\';');
                  }
              } else {
                  eval(elemObjName + '.' + value + ' = \'' + element['value'].replace("'", "\\'") + '\';');
              }
          }
      });
  });

  return jsonOutput;
}

// get form
var form = document.querySelector("#payment-form");

// Affiche bouton Paypal
paypal.Buttons({
  style: {
      size: 'responsive',
  },
  createOrder: function() {
    return fetch('{{ url_create }}', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "X-CSRFToken": csrftoken
      },
      body: JSON.stringify(Object.fromEntries(new FormData(form)))
    }).then(function(res) {
      return res.json();
    }).then(function(data) {
      return data.orderID; // Use the same key name for order ID on the client and server
    });
  },
  onApprove: function(data) {
    changeLoadingState(true);

    return fetch(`{{ url_capture }}?orderID=${data.orderID}`, {
      headers: {
        'content-type': 'application/json'
      }
    }).then(function(res) {
      return res.json();
    }).then(function() {
      location.href = "{{ url_next }}";
      return false;
    })
  }
}).render('#paypal-button-container');

// Show a spinner on payment submission
var changeLoadingState = function(isLoading) {
  if (isLoading) {
    document.querySelector("#paypal-button-container").classList.add("d-none");
    document.querySelector("#spinner").classList.remove("d-none");
  } else {
    document.querySelector("#paypal-button-container").classList.remove("d-none");
    document.querySelector("#spinner").classList.add("d-none");
  }
};

var check_accept = function (elt) {
  if(!elt.checked){
    document.querySelector("#order").classList.add("d-none");
    document.querySelector("#order").classList.remove("d-inline-flex");
  } else {
    document.querySelector("#order").classList.add("d-inline-flex");
    document.querySelector("#order").classList.remove("d-none");
  }
};

$("#id_accept").on("change", function() {check_accept(this)});