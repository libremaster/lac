// A reference to Stripe.js
var stripe = Stripe('{{ stripe_publishable_key }}');

// **** CSRF protection
var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
// ****

// convert FORM to JSON
$.fn.formToJson = function () {
  form = $(this);

  var formArray = form.serializeArray();
  var jsonOutput = {};

  $.each(formArray, function (i, element) {
      var elemNameSplit = element['name'].split('[');
      var elemObjName = 'jsonOutput';

      $.each(elemNameSplit, function (nameKey, value) {
          if (nameKey != (elemNameSplit.length - 1)) {
              if (value.slice(value.length - 1) == ']') {
                  if (value === ']') {
                      elemObjName = elemObjName + '[' + Object.keys(eval(elemObjName)).length + ']';
                  } else {
                      elemObjName = elemObjName + '[' + value;
                  }
              } else {
                  elemObjName = elemObjName + '.' + value;
              }

              if (typeof eval(elemObjName) == 'undefined')
                  eval(elemObjName + ' = {};');
          } else {
              if (value.slice(value.length - 1) == ']') {
                  if (value === ']') {
                      eval(elemObjName + '[' + Object.keys(eval(elemObjName)).length + '] = \'' + element['value'].replace("'", "\\'") + '\';');
                  } else {
                      eval(elemObjName + '[' + value + ' = \'' + element['value'].replace("'", "\\'") + '\';');
                  }
              } else {
                  eval(elemObjName + '.' + value + ' = \'' + element['value'].replace("'", "\\'") + '\';');
              }
          }
      });
  });

  return jsonOutput;
}

// get form
var form = document.querySelector("#payment-form");

form.addEventListener("submit", function(event) {
  event.preventDefault();
  changeLoadingState(true);

  //const form_data = $(this).formToJson();

  // Place order
  (async () => {
    const rawResponse = await fetch("{{ url }}", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "X-CSRFToken": csrftoken
      },
      body: JSON.stringify(Object.fromEntries(new FormData(form)))
    });

    const content = await rawResponse.json();

    if ('checkout_session_id' in  content) {
      // sending client to Stripe
      stripe.redirectToCheckout({
        sessionId: content.checkout_session_id
      }).then(function (result) {
        showError(result.error.message);
      });
    }

    if ('form_invalid_message' in  content) {
      changeLoadingState(false);
      displayErrMessage(content.form_invalid_message);
    }

    //console.log(content);
  })();

});

// Display error message
var showError = function(errorMsgText) {
  changeLoadingState(false);
  var errorMsg = document.querySelector(".sr-field-error");
  errorMsg.textContent = errorMsgText;
  setTimeout(function() {
    errorMsg.textContent = "";
  }, 4000);
};

// Show a spinner on payment submission
var changeLoadingState = function(isLoading) {
  if (isLoading) {
    document.querySelector("#payment-form button[type=submit]").disabled = true;
    document.querySelector("#payment-form #spinner").classList.remove("d-none");
  } else {
    document.querySelector("#payment-form button[type=submit]").disabled = false;
    document.querySelector("#payment-form #spinner").classList.add("d-none");
  }
};

// display Error Message
var displayErrMessage = function(message) {
  document.querySelector("#payment-form #errormessage").innerHTML = message;
  document.querySelector("#payment-form #errormessage").classList.remove("d-none");
};
