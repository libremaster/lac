# -*- coding: UTF-8 -*-

# python
import urllib.parse as urllib_parse

# django
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

# lib
from lac_dolibarr.lib import api as dol_api

# models
from lac_dolibarr import models as lac_dolibarr_models

# utils
def get_tier(dolibarr_socid, get_dol=True):
    if get_dol:
        dol_tier = dol_api.thirdparty_get(socid=dolibarr_socid)
        dol_tier['email_encoded'] = urllib_parse.quote(dol_tier['email'])
    else:
        dol_tier = None

    try:
        dj_tier = lac_dolibarr_models.Tier.objects.get(dolibarr_tier_id=int(dolibarr_socid))
    except ObjectDoesNotExist:
        dj_tier = lac_dolibarr_models.Tier.objects.create(dolibarr_tier_id=int(dolibarr_socid))
        dj_tier.save()

    return (dol_tier, dj_tier)

def get_contract(dolibarr_contract_id, socid=None, get_dol=True):
    if get_dol:
        dol_contract = dol_api.contract_get(contractid=dolibarr_contract_id)
        socid = dol_contract['socid']
    else:
        dol_contract = None

    try:
        dj_contract = lac_dolibarr_models.Contract.objects.get(dolibarr_contract_id=int(dolibarr_contract_id))
    except ObjectDoesNotExist:
        dj_contract = lac_dolibarr_models.Contract.objects.create(dolibarr_contract_id=int(dolibarr_contract_id))
        dj_contract.save()

    if not dj_contract.tier:
        if not socid:
            dol_contract = dol_api.contract_get(contractid=dolibarr_contract_id)
            socid = dol_contract['socid']
        (trash, dj_tier) = get_tier(socid, get_dol=False)
        dj_contract.tier = dj_tier
        dj_contract.save()

    return (dol_contract, dj_contract)

def get_contract_line(dj_contract, dolibarr_line_id=None, get_dol=True, dol_contract_line=None):
    if get_dol:
        dol_contract_line = dol_api.contract_line_get(contractid=dj_contract.dolibarr_contract_id, lineid=dolibarr_line_id)
    if dol_contract_line:
        dolibarr_line_id = dol_contract_line['id']

    try:
        dj_contract_line = lac_dolibarr_models.ContractLine.objects.get(contract=dj_contract, dolibarr_line_id=int(dolibarr_line_id))
        if dol_contract_line:
            dj_update = False
            if dj_contract_line.date_start != dol_contract_line['date_start']:
                dj_contract_line.date_start = dol_contract_line['date_start']
                dj_update = True
            if dj_contract_line.date_end != dol_contract_line['date_end']:
                dj_contract_line.date_end = dol_contract_line['date_end']
                dj_update = True
            if dj_update:
                dj_contract_line.save()
    except ObjectDoesNotExist:
        if not dol_contract_line:
            dol_contract_line = dol_api.contract_line_get(contractid=dj_contract.dolibarr_contract_id, lineid=dolibarr_line_id)
        service = lac_dolibarr_models.Service.objects.get(dolibarr_product_id=int(dol_contract_line['fk_product']))
        dj_contract_line = lac_dolibarr_models.ContractLine.objects.create(
            service=service, 
            contract=dj_contract,
            dolibarr_line_id=int(dolibarr_line_id), 
            date_start=dol_contract_line['date_start'], 
            date_end=dol_contract_line['date_end'])
        dj_contract_line.save()
        for category in service.category.all():
            for agreement in category.agreements.all():
                dj_contract_line.agreement.add(agreement)

    return (dol_contract_line, dj_contract_line)

def dolibarr_paginate_param(request, default_paginate=settings.PAGINATE_BY):
    param = {}

    param['limit'] = int(request.GET.get('limit', default_paginate))    
    param['page'] = int(request.GET.get('page', 0))
    param['sortorder'] = request.GET.get('sortorder', 'DESC')
    param['sortfield'] = request.GET.get('sortfield', 't.rowid')

    return param

def dolibarr_paginate_ctx(objs, param, request, ctx):
    nb_result = len(objs)

    # pour afficher ou non le bouton précédent / suivant
    bouton_before = True
    bouton_after = True

    # numéro de page en partant de 0
    btn_a_offset = 0
    btn_b_offset = 0

    # pas assez de résultat, pas de bouton suivant
    if nb_result < int(param['limit']):
        bouton_after = False
    else:
        btn_a_offset = int(param['page']) + 1

    # pas assez de résultat et pas d'offset précisé donc pas de bouton précédent
    if 'page' not in param or int(param['page']) == 0:
        bouton_before = False
    else:
        # calcul offset précédent
        btn_b_offset = int(param['page']) - 1
        if btn_b_offset < 0:
            btn_b_offset = 0

    # --------
    # make ctx
    # --------
    # limit
    ctx['limit'] = str(param['limit'])

    # offset
    ctx['query_a'] = '&page=' + str(btn_a_offset)
    ctx['query_b'] = '&page=' + str(btn_b_offset)

    # pour afficher ou non le bouton précédent / suivant
    ctx['bouton_before'] = bouton_before
    ctx['bouton_after'] = bouton_after

    return ctx

def get_product_param(line):
    name = ''
    description = ''
    sku = ''

    # produit
    if 'label' in line and line['label'] is not None:
        name = line['label']
        description = line['desc']
        sku = line['ref']
    elif 'product_label' in line and line['product_label'] is not None:
        name = line['product_label']
        description = line['description']
        sku = line['product_ref']
    else:
        name = 'SERVICE'
        description = line['description']

    return (name, description, sku)

def get_product_price_unit(line):
    if 'multicurrency_subprice' in line and line['multicurrency_subprice'] is not None:
        price = float(line['multicurrency_subprice'])
        tva = float(line['multicurrency_total_tva'])
    elif 'subprice' in line and line['subprice'] is not None:
        price = float(line['subprice'])
        tva = float(line['total_tva'])

    # la quantité peut être à la fois un nombre de produits ou une portion, on distingue les deux :
    # en cas de portion (service rendu fraction d'un taux horaire par exemple), on passe la quantité à 1 et on recalcule un prix final
    if line['qty'].is_integer():
        qty = int(line['qty'])
    else:
        price = price * line['qty']
        tva = tva * line['qty']
        qty = 1

    # on return float, float, int
    return (price, tva, qty)
