# -*- coding: UTF-8 -*-

# python
from restapi_client import rapi, ResponseError as rapi_ResponseError

import datetime
import pytz, re
from dateutil.relativedelta import relativedelta, MO
from decimal import Decimal, ROUND_HALF_UP, ROUND_UP
import urllib.parse as urllib_parse

# django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.cache import cache
from django.utils import timezone

# lib
from lac_dolibarr.lib import utils as lac_dolibarr_utils

# tools
def get_delta(unit, value):
    delta = None
    unit_str = None

    # calcul durée
    if unit == 's':
        unit_str = 'secondes'
        delta = relativedelta(seconds=int(value))
    if unit == 'i':
        unit_str = 'minutes'
        delta = relativedelta(minutes=int(value))
    if unit == 'h':
        unit_str = 'heures'
        delta = relativedelta(hours=int(value))
    if unit == 'd':
        unit_str = 'jours'
        delta = relativedelta(days=int(value))
    if unit == 'w':
        unit_str = 'semaines'
        delta = relativedelta(weeks=int(value))
    if unit == 'm':
        unit_str = 'mois'
        delta = relativedelta(months=int(value))
    if unit == 'y':
        unit_str = 'années'
        delta = relativedelta(years=int(value))

    return (delta, unit_str)

def return_list(fct, *args, **kwargs):
    try:
        return fct(*args, **kwargs)
    except rapi_ResponseError as error:
        if error.response.status_code == 404:
            return []
        else:
            raise rapi_ResponseError(error.response)

def convert_dt(value):     
    if isinstance(value, int):
        return datetime.datetime.fromtimestamp(value, tz=timezone.utc).astimezone(tz=pytz.timezone('Europe/Paris'))
    elif value == '':
        return None
    else:
        return value

def reconvert_dt_put(value):
    if isinstance(value, datetime.datetime):
        return int(value.timestamp())
    else:
        return value

def convert_price(value):
    if isinstance(value, str):
        return Decimal(value).quantize(Decimal('.01'), rounding=ROUND_UP)
    else:
        return value

def reconvert_price_put(value):
    if isinstance(value, Decimal):
        return str(value)
    else:
        return value

def convert_bool(value):
    if isinstance(value, str):
        if value == "0":
            return False
        else:
            return True
    else:
        return value

def convert_int(value):
    if isinstance(value, str):
        return int(value)
    else:
        return value

def convert_float(value):
    if isinstance(value, str):
        value = float(value)
        if value.is_integer():
            return int(value) 
        return value
    else:
        return value

def convert_linkkey(obj, key):
    dol_ids = []
    if isinstance(obj['linkedObjectsIds'], dict):
        for ok, ov in obj['linkedObjectsIds'].items():
            if ok == key:
                for ck, cv in ov.items():
                    dol_ids.append(cv)
    return dol_ids

def convert_contract_line(line):
    for dt in ['date_start','date_start_real', 'date_end', 'date_end_real']:
        line[dt] = convert_dt(line[dt])
    for prix in ['total_ttc', 'total_ht', 'total_tva']:
        line[prix] = convert_price(line[prix])
    if isinstance(line['array_options'], dict):
        if 'options_renew_auto' in line['array_options']:
            line['array_options']['options_renew_auto'] = convert_bool(line['array_options']['options_renew_auto'])
        if 'options_duration_unit' in line['array_options'] and line['array_options']['options_duration_unit'] and 'options_duration_value' in line['array_options'] and line['array_options']['options_duration_value']:
            line['delta'] = get_delta(line['array_options']['options_duration_unit'], line['array_options']['options_duration_value'])
        if 'options_mode_reglement' in line['array_options']:
            if line['array_options']['options_mode_reglement'] == str(settings.DOLIBARR_STRIPE_PAIEMENT_ID):
                line['mode_paiement_cb_stripe'] = True
            if line['array_options']['options_mode_reglement'] == str(settings.DOLIBARR_VIR_PAIEMENT_ID):
                line['mode_paiement_vir'] = True
        if 'options_cond_reglement' in line['array_options'] and line['array_options']['options_cond_reglement']:
            line['cond_reglement'] = setup_dictionnary_list(dic='payment_terms', sqlfilters='(t.rowid='+line['array_options']['options_cond_reglement']+')')[0]
    else:
        line['array_options'] = {}
    return line

def convert_contract(contract):
    for contract_line in contract['lines']:
        contract_line = convert_contract_line(contract_line)
    #contract = convert_linkkey(contract, 'facture')
    contract['invoice_dol_ids'] = convert_linkkey(contract, 'facture')
    contract['order_dol_ids'] = convert_linkkey(contract, 'commande')
    return contract

def reconvert_contract_line_put(line):
    for dt in ['date_start','date_start_real', 'date_end', 'date_end_real']:
        line[dt] = reconvert_dt_put(line[dt])
    for prix in ['total_ttc', 'total_ht', 'total_tva']:
        line[prix] = reconvert_price_put(line[prix])

    del line['delta']
    return line

def convert_invoice_line(line):
    for prix in ['multicurrency_total_ttc', 'multicurrency_total_tva', 'multicurrency_total_ht', 'multicurrency_subprice', 'total_ttc', 'total_localtax2', 'total_localtax1', 'total_tva', 'total_ht']:
        line[prix] = convert_price(line[prix])
    for k_float in ['qty']:
        line[k_float] = convert_float(line[k_float])

    return line

def convert_invoice(invoice):
    invoice['date'] = datetime.datetime.fromtimestamp(invoice['date'], tz=timezone.utc).astimezone(tz=pytz.timezone('Europe/Paris'))
    invoice['date_lim_reglement'] = datetime.datetime.fromtimestamp(invoice['date_lim_reglement'], tz=timezone.utc).astimezone(tz=pytz.timezone('Europe/Paris'))
    for prix in ['remaintopay', 'multicurrency_total_ttc']:
        invoice[prix] = convert_price(invoice[prix])
    invoice['payments'] = invoice_payments_list(facid=invoice['id'])
    for payment in invoice['payments']:
        if payment['type'] == 'PP':
            invoice['mode_reglement_paypal'] = True
        if payment['type'] == 'CB':
            invoice['mode_reglement_stripe'] = True
        #stripe_search = re.search('^pi_(.*)', payment['num'], re.IGNORECASE)
        #if stripe_search:
        #    invoice['mode_reglement_stripe'] = True
    invoice['order_dol_ids'] = convert_linkkey(invoice, 'commande')
    invoice['contract_dol_ids'] = convert_linkkey(invoice, 'contrat')
    for invoice_line in invoice['lines']:
        invoice_line = convert_invoice_line(invoice_line)

    return invoice

def convert_order_line(line, order=None):
    for dt in ['date_validation', 'date_creation', 'date_modification']:
        line[dt] = convert_dt(line[dt])
    for prix in ['multicurrency_total_ttc', 'multicurrency_total_tva', 'multicurrency_total_ht', 'multicurrency_subprice', 'total_ttc', 'total_localtax2', 'total_localtax1', 'total_tva', 'total_ht']:
        line[prix] = convert_price(line[prix])
    for k_float in ['qty']:
        line[k_float] = convert_float(line[k_float])
    if isinstance(line['array_options'], dict):
        if 'options_renew_auto' in line['array_options']:
            line['array_options']['options_renew_auto'] = convert_bool(line['array_options']['options_renew_auto'])
        if 'options_duration_unit' in line['array_options'] and line['array_options']['options_duration_unit'] and 'options_duration_value' in line['array_options'] and line['array_options']['options_duration_value']:
            line['delta'] = get_delta(line['array_options']['options_duration_unit'], line['array_options']['options_duration_value'])
    else:
        line['array_options'] = {}
        line['delta'] = None

    # recherche de la ligne de contrat associée si disponible
    line['dj_contract_line'] = None
    for dol_contract_id in order['contract_dol_ids']:
        (dol_contract, dj_contract) = lac_dolibarr_utils.get_contract(dolibarr_contract_id=dol_contract_id)
        for contract_line in dol_contract['lines']:
            (dol_contract_line, dj_contract_line) = lac_dolibarr_utils.get_contract_line(dj_contract=dj_contract, dolibarr_line_id=contract_line['id'])
            if dol_contract_line['fk_product'] == line['fk_product']:
                line['dj_contract_line'] = dj_contract_line

    return line

def convert_order(order):
    # STATUS_CANCELED = -1;
	# STATUS_DRAFT = 0;
	# STATUS_VALIDATED = 1;
	# STATUS_SHIPMENTONPROCESS = 2;
    # STATUS_CLOSED = 3;

    order['invoice_dol_ids'] = convert_linkkey(order, 'facture')
    order['contract_dol_ids'] = convert_linkkey(order, 'contrat')

    for boole in ['billed']:
        order[boole] = convert_bool(order[boole])
    if order['cond_reglement'] is None:
        order['cond_reglement'] = 'Gratuit'
    if order['mode_reglement'] is None:
        order['mode_reglement'] = 'Aucun'
    for dt in ['date','date_commande', 'date_livraison', 'date_creation', 'date_modification']:
        order[dt] = convert_dt(order[dt])
    for prix in ['multicurrency_total_ttc', 'multicurrency_total_tva', 'multicurrency_total_ht', 'multicurrency_tx', 'total_ttc', 'total_localtax2', 'total_localtax1', 'total_tva', 'total_ht']:
        order[prix] = convert_price(order[prix])
    for order_line in order['lines']:
        order_line = convert_order_line(order_line, order)
    return order

def convert_product(line):
    for boole in ['status', 'status_buy']:
        line[boole] = convert_bool(line[boole])
    for prix in ['price', 'localtax1', 'localtax2', 'price_ttc', 'total_ttc', 'total_ht', 'total_tva', 'price_min', 'price_min_ttc', 'total_localtax1', 'total_localtax2']:
        if prix in line:
            line[prix] = convert_price(line[prix])
    if 'duration_unit' in line and 'duration_value' in line:
        if line['duration_value'] != '':
            line['delta'] = get_delta(line['duration_unit'], line['duration_value'])
    return line

def return_cache(cache_key_prefix, cache_key_suffix, cached, sec, fct, *args, **kwargs):

    if cached:
        key = 'dol_' + cache_key_prefix + '_' + cache_key_suffix
        obj = cache.get(key)
        if obj is None:
            obj = fct(*args, **kwargs)
            cache.set(key, obj, sec)
        return obj
    else:
        return fct(*args, **kwargs)

def delete_cache(cache_key_prefix, cache_key_suffix):
    cache.delete('dol_' + cache_key_prefix + '_' + cache_key_suffix)

# TIER

def thirdparties_list(socids = None, sqlfilters = '', cached=True, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    datas = {}
    if socids == -1:
        socids = ''
    elif socids == '' or socids is None:
        socids = '0'
    for rowid in socids:
        if sqlfilters == '':
            sqlfilters = '(t.rowid=' + str(rowid) + ')'
        else:
            sqlfilters += ' or (t.rowid=' + str(rowid) + ')'
    if len(sqlfilters) > 0:
        datas['sqlfilters'] = sqlfilters
    
    return return_cache('tiers', str(socids), cached, 60, return_list, client.get, '/thirdparties', datas=datas)

def thirdparty_get(socid, cached=True, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return return_cache('tier', str(socid), cached, 30, client.get, '/thirdparties/' + str(socid))

def thirdparty_post(datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.post('/thirdparties/', datas_json=datas)

def thirdparty_put(socid, datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('tier', str(socid))

    return client.put('/thirdparties/' + str(socid), datas_json=datas)

def thirdparty_get_byemail(email, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.get('/thirdparties/byEmail/' + urllib_parse.quote(email))

# SETUP

def setup_dictionnary_list(dic, sortfield='', sqlfilters = '', cached=True, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    datas = {'sortorder': 'ASC'}

    if sortfield != '':
        datas['sortfield'] = sortfield
    
    # sql
    sql = '(t.active:=:1)'
    if sqlfilters != '':
        sql += ' AND (' + sqlfilters + ')'
    datas['sqlfilters'] = sql

    return return_cache('setup_dictionnary', dic, cached, 60, return_list, client.get, '/setup/dictionary/' + dic, datas=datas)

def setup_company(debug=False):

    return return_cache('setup_company', '', cached, 60, client.get, '/setup/company')

# FACTURE

def invoices_list(socid = None, paginate_param = {'sortfield': 't.rowid', 'sortorder': 'DESC', 'limit': 100, 'page': 0}, sqlfilters = '', cached=True, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    datas = paginate_param

    # socid
    thirdparty_ids = ''
    if socid == -1:
        thirdparty_ids = ''
    elif socid == '' or socid is None:
        thirdparty_ids = '0'
    else:
        thirdparty_ids = str(socid)
    if thirdparty_ids != '':
        datas['thirdparty_ids'] = thirdparty_ids

    # sql
    sql = '(t.fk_statut IN (1) OR t.fk_statut IN (2))'
    if sqlfilters != '':
        sql += ' AND (' + sqlfilters + ')'
    datas['sqlfilters'] = sql

    # call
    invoices = return_cache('invoices', str(socid) + '_' + str(paginate_param['page']), cached, 60, return_list, client.get, '/invoices', datas=datas)
    for invoice in invoices:
        invoice = convert_invoice(invoice)

    return invoices

def invoice_get(facid, cached=True, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    invoice = return_cache('invoice', str(facid), cached, 60, client.get, '/invoices/' + str(facid))
    return convert_invoice(invoice)

def invoice_post(datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('invoice', str(datas['socid']))

    return client.post('/invoices/', datas_json=datas)

def invoice_put(facid, datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('invoice', str(facid))

    return client.put('/invoices/' + str(facid), datas_json=datas)

def invoice_line_post(facid, datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('invoice', str(facid))

    return client.post('/invoices/' + str(facid) + '/lines', datas_json=datas)

def invoice_line_put(facid, lineid, datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('invoice', str(facid))

    return client.put('/invoices/' + str(facid) + '/lines/' + str(lineid), datas_json=datas)

def invoice_validate(facid, datas = {}, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('invoice', str(facid))

    return client.post('/invoices/' + str(facid) + '/validate', datas_json=datas)

def invoice_payments_list(facid, cached=True, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return return_cache('invoice_payments', str(facid), cached, 60, client.get, '/invoices/' + str(facid) + '/payments')

def invoice_paymentsdistributed_post(datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    ret = client.post('/invoices/paymentsdistributed', datas_json=datas)

    for facid, val in datas['arrayofamounts'].items():
        delete_cache('invoice_payments', str(facid))
        delete_cache('invoice', str(facid))

    return ret

def invoice_createfromorder(orderid, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.post('/invoices/createfromorder/' + str(orderid))

# COMMANDE

def orders_list(socid = None, sqlfilters = '', debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    datas = {'sortfield': 't.rowid', 'sortorder': 'DESC', 'limit': 100}

    # socid
    thirdparty_ids = ''
    if socid == -1:
        thirdparty_ids = ''
    elif socid == '' or socid is None:
        thirdparty_ids = '0'
    else:
        thirdparty_ids = str(socid)
    if thirdparty_ids != '':
        datas['thirdparty_ids'] = thirdparty_ids

    # sql
    sql = '(t.fk_statut IN (1) OR t.fk_statut IN (2) OR t.fk_statut IN (3))'
    if sqlfilters != '':
        sql += ' AND (' + sqlfilters + ')'
    datas['sqlfilters'] = sql

    # call
    orders = return_list(client.get, '/orders', datas=datas)
    for order in orders:
        order = convert_order(order)

    return orders

def order_get(orderid, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return convert_order(client.get('/orders/' + str(orderid)))

def order_post(datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.post('/orders/', datas_json=datas)

def order_validate(orderid, datas = {"idwarehouse": 0, "notrigger": 0 }, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.post('/orders/' + str(orderid) +'/validate', datas_json=datas)

def order_put(orderid, datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.put('/orders/' + str(orderid), datas_json=datas)

def order_line_put(orderid, lineid, datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    client.put('/orders/' + str(orderid) + '/lines/' + str(lineid), datas_json=datas)

# DOCUMENT

def documents_download(modulepart, original_file, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.get('/documents/download', datas={'modulepart': modulepart, 'original_file': original_file})

def documents_builddoc(modulepart, original_file, doctemplate, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.put('/documents/builddoc', datas_json={'modulepart': modulepart, 'original_file': original_file, 'doctemplate': doctemplate, 'langcode': 'fr_FR'})

# CONTRAT

def contracts_list(socid = None, sqlfilters = '', debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    datas = {'sortfield': 't.rowid', 'sortorder': 'DESC', 'limit': 100}

    # socid
    thirdparty_ids = ''
    if socid == -1:
        thirdparty_ids = ''
    elif socid == '' or socid is None:
        thirdparty_ids = '0'
    else:
        thirdparty_ids = str(socid)
    if thirdparty_ids != '':
        datas['thirdparty_ids'] = thirdparty_ids

    # sql
    sql = ''
    if sqlfilters != '':
        if sql != '':
            sql += ' AND (' + sqlfilters + ')'
        else:
            sql = sqlfilters
    datas['sqlfilters'] = sql

    # call
    contracts = return_list(client.get, '/contracts', datas=datas)
    for contract in contracts:
        contract = convert_contract(contract)

    return contracts

def contract_post(datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.post('/contracts/', datas_json=datas)

def contract_line_post(contractid, datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('contract_lines', str(contractid))

    return client.post('/contracts/' + str(contractid) + '/lines', datas_json=datas)

def contract_line_get(contractid, lineid, cached=True, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    lines = return_cache('contract_lines', str(contractid), cached, 60, client.get, '/contracts/' + str(contractid) + '/lines')

    line_contract = None
    for line in lines:
        if line['id'] == str(lineid):
            line_contract = convert_contract_line(line)

    return line_contract

def contract_line_put(contractid, lineid, datas, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('contract_lines', str(contractid))

    datas = reconvert_contract_line_put(datas)

    # correction
    datas['fk_fourn_price'] = datas['fk_fournprice']
    datas['date_ouveture_prevue'] = datas['date_start']
    datas['date_ouverture'] = datas['date_start_real']
    datas['date_fin_validite'] = datas['date_end']
    datas['date_cloture'] = datas['date_end_real']

    client.put('/contracts/' + str(contractid) + '/lines/' + str(lineid), datas_json=datas)

def contract_line_activate(contractid, lineid, datestart, dateend, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    delete_cache('contract_lines', str(contractid))

    datas = {}
    datas['datestart'] = reconvert_dt_put(datestart)
    datas['dateend'] = reconvert_dt_put(dateend)

    client.put('/contracts/' + str(contractid) + '/lines/' + str(lineid) + '/activate', datas_json=datas)

def contract_get(contractid, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    contract = client.get('/contracts/' + str(contractid))

    return convert_contract(contract)

def contract_validate(contractid, datas = {"idwarehouse": 0, "notrigger": 0}, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return client.post('/contracts/' + str(contractid) +'/validate', datas_json=datas)

# CATEGORIE

def categories_list(debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return return_list(client.get, '/categories', datas={'sortfield': 't.rowid', 'sortorder': 'ASC', 'limit': 100})

# PRODUIT

def products_list(debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    products = client.get('/products', datas={'sortfield': 't.ref', 'sortorder': 'ASC', 'limit': 100, 'mode': 2})

    for product in products:
        product = convert_product(product)

    return products

def products_get(proid, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return convert_product(client.get('/products/' + str(proid)))

def product_categories_list(proid, debug=False):
    client = rapi(settings.DOLIBARR_API_URL, hoster_api_key_kname='DOLAPIKEY', hoster_api_key=settings.DOLIBARR_API_KEY, debug=debug)

    return return_list(client.get, '/products/' + str(proid) + '/categories', datas={'sortfield': 's.rowid', 'sortorder': 'ASC'})
